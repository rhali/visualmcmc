package se.cbb.vmcmc.gui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class MCMCProgressBar extends JPanel implements ActionListener, PropertyChangeListener {
	private static final long serialVersionUID = 1L;
	private JProgressBar progressBar;
	private JTextArea taskOutput;
	private int progress;
	private String event;

	public MCMCProgressBar(int progress) {
		super(new BorderLayout());

        //Create the demo's UI.
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(progress);
        progressBar.setStringPainted(true);

        taskOutput = new JTextArea(5, 20);
        taskOutput.setMargin(new Insets(5,5,5,5));
        taskOutput.setEditable(false);
        taskOutput.append(String.format("Started reading file.\n"));

        JPanel panel = new JPanel();
        panel.add(progressBar);

        add(panel, BorderLayout.PAGE_START);
        add(new JScrollPane(taskOutput), BorderLayout.CENTER);
        setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setVisible(true);
        this.update(this.getGraphics());
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		progressBar.setValue(progress);
        taskOutput.append(String.format("Finished %s.\n", event));
        if(event == "loading file") 
        	taskOutput.append(String.format("Computing overview panel. Please wait.\n", event));
        this.update(this.getGraphics());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	}
	
	public void setProgress(int progress, String event) {
		this.progress = progress;
		this.event = event;
		propertyChange(null);
	}
}
