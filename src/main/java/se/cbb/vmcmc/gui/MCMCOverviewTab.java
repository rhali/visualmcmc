package se.cbb.vmcmc.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import se.cbb.vmcmc.libs.MCMCDataContainer;
import se.cbb.vmcmc.libs.MCMCMath;

/**
 * MCMCMainTab: Tab panel for the main tab. Central to all numerical analysis of a file.
 */
public class MCMCOverviewTab extends MCMCStandardTab {
	private static final long 	serialVersionUID = 1L;
	private DefaultTableModel 	model;
	private JTable 				table;
	private Thread[] 			workerThreads;
	private Lock 				displaypanelLock;
	private MCMCDisplayPanel 	scrollpane1;
	private MCMCDisplayPanel 	scrollpane2;
	private int 				EssMax;
	private int 				overallConvergenceGR;
	private int 				overallGeweke;

	private class MCMCTableModel extends DefaultTableModel{
		private static final long serialVersionUID = 1L;

		public boolean isCellEditable(int rowIndex,int columnIndex) {
			return false;
		}
	}

	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public MCMCOverviewTab(MCMCDataContainer datacontainer) {
		super();
		displaypanelLock = new ReentrantLock();
		this.datacontainer = datacontainer;
		
		scrollpane1 = new MCMCDisplayPanel("Trace Convergence Diagnostic");
		scrollpane1.setLayout(new FlowLayout());
		scrollpane1.setMinimumSize(new Dimension(300, 0));
		scrollpane1.setPreferredSize(new Dimension(300, 0));
		scrollpane1.setBackground(new Color(0xFFEEEEFF));

		scrollpane1.add(Box.createRigidArea(new Dimension(0, 7)));
		
		scrollpane1.addComponent("Geweke Last Burnin : ", new JLabel(), new Dimension(275, 18));
		scrollpane1.addComponent("  Gelman-Rubin LB : ", new JLabel(), new Dimension(275, 18));

		scrollpane2 = new MCMCDisplayPanel("Trace Burn-in Estimators");
		scrollpane2.setLayout(new FlowLayout());
		scrollpane2.setMinimumSize(new Dimension(300, 0));
		scrollpane2.setPreferredSize(new Dimension(300, 0));
		scrollpane2.setBackground(new Color(0xFFEEEEFF));

		scrollpane2.add(Box.createRigidArea(new Dimension(0, 7)));
		
		scrollpane2.addComponent("ESS Last Burnin : ", new JLabel(), new Dimension(275, 18));
		
		scrollpane2.add(Box.createRigidArea(new Dimension(0, 7)));
		scrollpane2.addComponent("  Normalized ESS : ", new JLabel(), new Dimension(275, 18));
		scrollpane2.add(Box.createRigidArea(new Dimension(0, 7)));
		scrollpane2.addComponent("  Standardized ESS : ", new JLabel(), new Dimension(275, 18));
		scrollpane2.add(Box.createRigidArea(new Dimension(0, 7)));
		
		model = new MCMCTableModel();
		table = new JTable(model);

		table.setCellSelectionEnabled(true);
		table.getTableHeader().setBackground(new Color(0xFFDDDDFF));
		table.getTableHeader().setResizingAllowed(false);
		table.getTableHeader().setReorderingAllowed(false);

		workerThreads = new Thread[5];

		for(int i=0; i<workerThreads.length; i++)
			workerThreads[i] = new Thread();
	}

	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public void addColumn(String name, List<?> data) 		{ model.addColumn(name, data.toArray());}
	public JTable getTable() 								{ return table; }
	public MCMCDataContainer getDataContainer() 			{ return datacontainer; }
	public void setEssMax(int ess) 							{ EssMax = ess; }
	public void setGeweke(int geweke) 						{ overallGeweke = geweke; }
	public void setConvergeneceGR(int overallConvergenceGR) { this.overallConvergenceGR = overallConvergenceGR; }

	/** updateDisplayPanels: Will calculate, format and display information for each display panel in the left panel. */
	public void updateDisplayPanels(){
		if(datacontainer != null) {
			final Object[] serie 		= datacontainer.getValueSerie(seriesID).toArray();
			final MCMCDataContainer container = datacontainer.deepCopy();
			final MCMCDataContainer container1 = datacontainer.deepCopy();

			for(int t=0; t<workerThreads.length; t++)
				workerThreads[t].interrupt();

			final Double[] data 	= new Double[serie.length];
			System.arraycopy(serie, 0, data, 0, serie.length);

			//Thread updating arithmetic mean.
			workerThreads[0] = new Thread() {
				public void run() {
					if(EssMax >= 0) {
						displaypanelLock.lock();
						scrollpane2.labels.get(0).setText(String.valueOf(EssMax));
						displaypanelLock.unlock();
					} else {
						displaypanelLock.lock();
						scrollpane2.labels.get(0).setText("Not converged");
						displaypanelLock.unlock();
					}
				}
			};
			workerThreads[0].start();

			//Thread updating arithmetic standard deviation.
			workerThreads[1] = new Thread() {
				public void run() {
					if(overallGeweke > 0) {
						displaypanelLock.lock();
						scrollpane1.labels.get(0).setText(String.valueOf(overallGeweke));
						displaypanelLock.unlock();
					} else {
						displaypanelLock.lock();
						scrollpane1.labels.get(0).setText("Not Converged");
						displaypanelLock.unlock();
					}
				}
			};
			workerThreads[1].start();
			
			//Thread updating arithmetic mean.
			workerThreads[2] = new Thread() {
				public void run() {
					if(overallConvergenceGR > -1) {
						displaypanelLock.lock();
						scrollpane1.labels.get(1).setText(" " + String.valueOf(overallConvergenceGR));
						displaypanelLock.unlock();
					}
					else {
						displaypanelLock.lock();
						scrollpane1.labels.get(1).setText(" Not Converged");
						displaypanelLock.unlock();
					}
				}
			};
			workerThreads[2].start();

			//Thread updating geometric mean
			workerThreads[3] = new Thread() {
				public void run() {
					int normalizedess = MCMCMath.calculateNormalizedESS(container);

					displaypanelLock.lock();
					scrollpane2.labels.get(1).setText(" " + String.valueOf(normalizedess));
					displaypanelLock.unlock();
				}
			};
			workerThreads[3].start();

			//Thread updating geometric standard deviation
			workerThreads[4] = new Thread() {
				public void run() {
					int standardizedess = MCMCMath.calculateStandardizedESS(container1);

					displaypanelLock.lock();
					scrollpane2.labels.get(2).setText(" " + String.valueOf(standardizedess));
					displaypanelLock.unlock();
				}
			};
			workerThreads[4].start();
		}
	}
	
	public MCMCDisplayPanel getOverallConvergencePanel() { return scrollpane1; }
	public MCMCDisplayPanel getOverallBurninPanel() { return scrollpane2; }
/* **************************************************************************** *
 * 							END OF CLASS										*
 * **************************************************************************** */
}
