package se.cbb.vmcmc.newickhandler;

public class NewickException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public NewickException (String msg) {
		super(msg);
	}
	
	public NewickException (Throwable cause) {
		super(cause);
	}
	
	public NewickException (String msg, Throwable cause) {
		super(msg, cause);
	}
}
