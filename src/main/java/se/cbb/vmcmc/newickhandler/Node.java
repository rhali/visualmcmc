package se.cbb.vmcmc.newickhandler;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Vector;

public class Node {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private String name 			= null;
	private String minimum 			= name;
	private String value 			= "0";
	private Vector<Node> childVec 	= new Vector<Node>();
	private BitSet splitVal;
	private Double supportVal;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public Node() 	{ }
	public Node(String distance) { 
		value = distance;
	}

	/* **************************************************************************** *
	 * 							CLASS PRIVATE FUNCTIONS								*
	 * **************************************************************************** */
	// toString method provided for testing purposes
	private void toString(Node n, StringBuffer sb) {
		if (n.numChildren() == 0) {
			sb.append(n.name);
			if(n.value != "0") {
				sb.append(":");
				sb.append(n.value);
			}
		} else {
			sb.append("(");
			toString(n.getChild(0), sb);
			for (int i = 1; i < n.numChildren(); i++) {
				sb.append(",");
				toString(n.getChild(i), sb);
			}
			sb.append(")");
			if(n.value != "0") {
				sb.append(":" + n.value);
			}
		}
	}
	
	private void toStringWithSupportVal(Node n, StringBuffer sb) {
		if (n.numChildren() == 0) {
			sb.append(n.name);
		} else {
			sb.append("(");
			toStringWithSupportVal(n.getChild(0), sb);
			for (int i = 1; i < n.numChildren(); i++) {
				sb.append(",");
				toStringWithSupportVal(n.getChild(i), sb);
			}
			Double temp = Double.valueOf(n.name);
			sb.append(")" + temp.toString());
		}
	}
	
	// toString method provided for testing purposes
	private void toStringWithoutLength(Node n, StringBuffer sb) {
		if (n.numChildren() == 0) {
			sb.append(n.name);
		} else {
			sb.append("(");
			toStringWithoutLength(n.getChild(0), sb);
			for (int i = 1; i < n.numChildren(); i++) {
				sb.append(",");
				toStringWithoutLength(n.getChild(i), sb);
			}
			sb.append(")");
		}
	}
	
	// toString method provided for testing purposes
	private void swaplengthwithname(Node n, StringBuffer sb) {
		if (n.numChildren() == 0) {
			sb.append(n.name);
		} else {
			sb.append("(");
			swaplengthwithname(n.getChild(0), sb);
			for (int i = 1; i < n.numChildren(); i++) {
				sb.append(",");
				swaplengthwithname(n.getChild(i), sb);
			}
			sb.append(")");
			if(n.value != "0") {
				if(n.value.length() > 5)
					sb.append(n.value.substring(0, 4));
				else
					sb.append(n.value);
			}
		}
	}

	private void reroot(Node root, ArrayList<Node> ListOfNodesInRerootOrder) {
		if(root.numChildren() != 0)
			reroot(root.getChild(0), ListOfNodesInRerootOrder);
		
		if (root.numChildren() == 0)
			ListOfNodesInRerootOrder.add(root);
		else if (root.numChildren() == 2)
			ListOfNodesInRerootOrder.add(root.getChild(1));
		else if(root.numChildren() == 3) {
			Node r = new Node();
			r.addChild(root.getChild(1));
			r.addChild(root.getChild(2));
			ListOfNodesInRerootOrder.add(r);
		}
	}

	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public void sortChildrenAlphabetically() {
		for(int i = 0; i < childVec.size(); i++)
			childVec.get(i).sortChildrenAlphabetically();
		
		for(int i = 0; i < childVec.size(); i++) {
			int minIndex = i;
			String min = childVec.get(i).getMinimum();
			for(int j = i+1; j < childVec.size(); j++) {
				if(childVec.get(j).getMinimum().compareTo(min) < 0) {
					minIndex = j;
					min = childVec.get(j).getMinimum();
				}
			}
			if(i != minIndex) {
				Node temp = childVec.get(i);
				childVec.set(i, childVec.get(minIndex));
				childVec.set(minIndex, temp);
			}
		}
		if(childVec.size() == 0) {
			minimum = name;
		} else {
			this.setMinimum(childVec.get(0).getMinimum());
		}
	}
	
	public Node rerootTree(Node root) {
		ArrayList<Node> list = new ArrayList<Node>();
		reroot(root, list);
		while (list.size() > 1) {
			Node temp = new Node();
			temp.addChild(list.get(list.size()-2));
			temp.addChild(list.get(list.size()-1));
			list.remove(list.get(list.size()-1));
			list.remove(list.get(list.size()-1));
			list.add(temp);
		}
		sortChildrenAlphabetically();
		return list.get(0);
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		toString(this, sb);
		return sb.toString();
	}
	
	public String toStringWithSupportVal() {
		StringBuffer sb = new StringBuffer();
		toStringWithSupportVal(this, sb);
		return sb.toString();
	}
	
	public String toStringWithoutLength() {
		StringBuffer sb = new StringBuffer();
		toStringWithoutLength(this, sb);
		return sb.toString() + ";";
	}
	
	public String toStringFromConsensus() {
		StringBuffer sb = new StringBuffer();
		swaplengthwithname(this, sb);
		return sb.toString() + ";";
	}
	public void setName(String name) 		{ this.name = name; }
	public void setMinimum (String minimum) { this.minimum = minimum; }
	public void addChild(Node n) 			{ childVec.add(n); }
	public int numChildren() 				{ return childVec.size(); }
	public String getName() 				{ return name; }
	public String getMinimum() 				{ return minimum; }
	public Node getChild(int i) 			{ return (Node) childVec.get(i); }
	public void setSplitName(BitSet split)  { splitVal = split; }
	public BitSet getSplitName()            { return splitVal; } 
	public Double getSupportValue()         { return supportVal; } 
	public void setSupportValue(Double supVal) { supportVal = supVal; } 
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
