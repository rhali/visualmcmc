package se.cbb.vmcmc.libs;

import java.io.File;
import java.util.List;

import se.cbb.vmcmc.libs.Parameters;

public class ParameterParser {
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	public static Triple<String, Integer, Double> getOptions(Parameters ps) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		String 					filename;
		int 					burnin = 0;
		double 					confidence;

		/* ******************** VARIABLE INITIALIZERS ***************************** */
		filename		 		= ps.files.get(0);
		try {
			burnin	 			= Integer.parseInt(ps.burnin);
		} catch (NumberFormatException e) {
			MCMCDataContainer datacontainer;
			try {
				datacontainer = MCMCFileReader.readMCMCFile(new File(filename), false, false);

				if(ps.burnin.contains("%")) {
					int percent = Integer.parseInt(ps.burnin.substring(0, ps.burnin.length() - 1));
					burnin = datacontainer.getNumLines() * percent/100;
				} else if(ps.burnin.equalsIgnoreCase("G")) {
					int overallConvergenceGeweke = 0;
					for (List<Double> valueSerie : datacontainer.getValueSeries()) {
						Object[] serie = valueSerie.toArray();
						int serieLength	= serie.length;

						if(serieLength < 100){
							burnin = 0;
						}

						int geweke = MCMCMath.calculateGeweke(serie);

						if (geweke != -1) {
							if(overallConvergenceGeweke != -1 && geweke > overallConvergenceGeweke) {
								overallConvergenceGeweke = geweke;
							}
						} else {
							overallConvergenceGeweke = -1;
						}
					}
					
					if(overallConvergenceGeweke != -1)
						burnin = overallConvergenceGeweke;
					else
						burnin = 0;
				} else if(ps.burnin.equalsIgnoreCase("GR")) {
					int overallGelmanRubin = 0;
					for (List<Double> valueSerie : datacontainer.getValueSeries()) {
						Object[] serie = valueSerie.toArray();
						int serieLength	= serie.length;

						if(serieLength < 100){
							burnin = 0;
						}

						int originalBurnin = 0;
						boolean gelmanRubin	= MCMCMath.GelmanRubinTest(serie, originalBurnin);
						while(gelmanRubin != true && originalBurnin < (0.5 * serie.length)) {
							originalBurnin = originalBurnin + 1;
							gelmanRubin	= MCMCMath.GelmanRubinTest(serie, originalBurnin);
						}
						if(gelmanRubin == true)	{
							if(overallGelmanRubin != -1 && originalBurnin > overallGelmanRubin)
								overallGelmanRubin = originalBurnin;
						} else
							overallGelmanRubin = -1;
					}
					
					if(overallGelmanRubin != -1)
						burnin = overallGelmanRubin;
					else
						burnin = 0;
				} else if(ps.burnin.equalsIgnoreCase("ESSM")) {
					if(datacontainer != null) {
						int overallConvergenceESS = 0;
						int ess;
						int numSeries = datacontainer.getNumValueSeries();
						if(numSeries != 0) {
							for (List<Double> i : datacontainer.getValueSeries()) {
								Object[] serie = i.toArray();
								int serieLength	= serie.length;

								if(serieLength < 100){
									overallConvergenceESS = -1;
									break;
								}

								ess = MCMCMath.calculateESSmax(serie);

								if(overallConvergenceESS != -1 && ess > overallConvergenceESS) 
									overallConvergenceESS = ess;
							}
						} 
						if(overallConvergenceESS == -1) 
							burnin = 0;
						else
							burnin = overallConvergenceESS;
					}
				} else if(ps.burnin.equalsIgnoreCase("SESS")) {
					int standardizedESS = MCMCMath.calculateStandardizedESS(datacontainer);
					burnin = standardizedESS;
				} else if(ps.burnin.equalsIgnoreCase("NESS")) {
					int normalizedESS = MCMCMath.calculateNormalizedESS(datacontainer);
					burnin = normalizedESS;
				} else {
					System.out.println("Unknown burnin option. Please give valid value.");
					burnin = 0;
				}
			} catch (Exception e1) {
				System.out.println(e1.getMessage());
				burnin = 0;
			}	
		}
		confidence		 		= Double.parseDouble(ps.confidence);
		
		/* ******************** FUNCTION BODY ************************************* */
		return new Triple<String, Integer, Double>(filename, burnin, confidence);
	}
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
