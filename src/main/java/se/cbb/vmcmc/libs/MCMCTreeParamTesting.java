package se.cbb.vmcmc.libs;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.commons.math3.stat.inference.ChiSquareTest;

/** @author Sayyed Auwn Muhammad 
 * @author Raja Hashim Ali*/
public class MCMCTreeParamTesting {
	private static int pos=0;
	private static int depth=0;
	private static String treeStr = null;
	private static String token = null;
	
	private static HashMap<String, Integer> name2id;
	private static HashMap<Integer, String> id2name;

	private static char readChar() {
		char chr;

		// Indicate End Of String (EOS) 
		if(pos == treeStr.length())
			return '\0';

		chr = treeStr.charAt(pos); 
		pos++;

		// keep track of parent depth
		if (chr == '(') depth++;
		if (chr == ')') depth--;

		return chr;
	}

	private static char readUntil(String stops) {
		char chr;
		token = "";

		while (true) {
			chr = readChar();

			if (chr == '\0')
				return chr;

			// compare char to stop characters
			for (int i=0 ; i<stops.length() ;  i++) {
				if (chr == stops.charAt(i))
					return chr;
			}
			token += chr;
		}
	}

	private static Node readNewickNode(ArrayList<Node> tree, Node parent) {
		char char1;
		Node node;

		//Read first Character
		if( (char1 = readChar()) == '\0' ) {
			System.out.println("Error: Not a valid Newick tree string ...");
			return null;
		}

		if (char1 == '(') {
			// read internal node
			int depth2 = depth;
			node = new Node();
			tree.add(node);
			node.id = tree.size()-1;
			if (parent != null)
				parent.addChild(node);

			// read all child nodes at this depth
			while (depth == depth2) {
				Node child = readNewickNode(tree, node);
				if (child==null) 
					return null;
			}

			// prepare for next read operation
			readUntil( "):,;" );
			return node;
		} else {
			// read leaf
			node = new Node();
			tree.add(node);
			node.id = tree.size()-1;
			if (parent != null)
				parent.addChild(node);

			// Read name
			if ( readUntil( ":)," ) == '\0'  )
				return null;
			token = char1 + token;
			node.name = token;
			return node;
		}
	}
	
	// Scanning through the newick string to extract the leaves name
	public static ArrayList<String> scannwStr(String nwStr) {
		ArrayList <String> leaves = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(nwStr, "(),"); 
		String temp; 
		while (st.hasMoreTokens() ) {
			temp = st.nextToken();
			if(temp.equals(";") ) 
				break;
			leaves.add(temp);
		}
		return leaves;
	}
	
	// Parse each newick tree string into splits and record them in the given splitMap
	public static void parseNwStr(String nwStr,  HashMap<List<Integer>,Integer> splitMap) {
		pos = 0;
		depth = 0;
		token = new String();
		treeStr = new String(nwStr);
		
		ArrayList<Node> tree = new ArrayList<Node>(); 
		HashMap<List<Integer>,Integer> splitSet = new HashMap<List<Integer>,Integer>();
		
		readNewickNode(tree, null);
		
	    for(int i=0; i<tree.size() ; i++ ) {
	    	Node n = tree.get(i);
	    	if(n.nchildren == 0 || n.parent == null) {
	    		continue;
	    	} else {
	    		ArrayList<Integer> list = new ArrayList<Integer>(); 
	    		getSplit(n,list);
	    		Collections.sort(list);
	    	
	    		ArrayList<Integer> compList = new ArrayList<Integer>();
				for(Integer id:id2name.keySet()){
					if(!list.contains(id))
						compList.add(id);
				}
				Collections.sort(compList);
				
				ArrayList<Integer> split;
				if( compList.size() < list.size() ) 
					split = compList;
				else if( compList.size() > list.size() ) 
					split = list;
				else {
					if( compList.get(0) < list.get(0))		
						split = compList;
					else
						split = list;
				}
	    	
				// Single split / tree
				if(!splitSet.containsKey(split))
					splitSet.put(split, 1);
				else
					continue;
				
				if(split.size() == 1)
					continue;
				
	    		if(splitMap.containsKey(split))
	    			splitMap.put(split, splitMap.get(split)+1);
	    		else
	    			splitMap.put(split, 1);
	    	}
	    }
	}
		
	//Recursive method to get 
	static void getSplit(Node node, ArrayList<Integer> list) {
		for (int i=0; i<node.nchildren; i++) {
			Node child = node.children.get(i);
			if (child.nchildren == 0) 
				list.add(name2id.get(child.name));
			else 
				getSplit(child,list);
		}
	}
	
	// This method will prune the splits for small size
	// For example: 
	// [0, 1, 2, 3, 8, 9, 10] U [4, 5, 6, 7, 11] -> [4, 5, 6, 7, 11]
	// For equal size split it will prefer split with starting indicies
	// For example: 
	// [0, 1, 2, 3, 10, 11] U [4, 5, 6, 7, 8, 9] -> [0, 1, 2, 3, 10, 11] 
	static void pruneMap(HashMap<List<Integer>,Integer> splitMap) {
		HashMap<List<Integer>,Integer> prunedMap = new HashMap<List<Integer>,Integer>();
		
		for(List<Integer> oldlist: splitMap.keySet()) {
			ArrayList<Integer> newlist = new ArrayList<Integer>();
			for(Integer id:id2name.keySet()) {
				if(!oldlist.contains(id))
					newlist.add(id);
			}
			Collections.sort(newlist);
		   
			if (splitMap.containsKey(newlist)) {
				if( newlist.size() < oldlist.size()) 
					prunedMap.put(newlist, splitMap.get(newlist) + splitMap.get(oldlist));
				else if (newlist.size() > oldlist.size()) 
					prunedMap.put(oldlist, splitMap.get(newlist) + splitMap.get(oldlist));
				else if (newlist.get(0) < oldlist.get(0))		
					prunedMap.put(newlist, splitMap.get(newlist) + splitMap.get(oldlist));
				else
					prunedMap.put(oldlist, splitMap.get(newlist) + splitMap.get(oldlist));					
			} else {
				if (newlist.size() < oldlist.size()) 
					prunedMap.put(newlist, splitMap.get(oldlist));
				else if (newlist.size() > oldlist.size()) 
					prunedMap.put(oldlist, splitMap.get(oldlist));
				else if(newlist.get(0) < oldlist.get(0))		
					prunedMap.put(newlist, splitMap.get(oldlist));
				else
					prunedMap.put(oldlist, splitMap.get(oldlist));
			}
		}
		
		splitMap.clear();
		splitMap.putAll(prunedMap);
	}
	
	// Assuming newick trees without lengths
	public static ArrayList<String> performTest(ArrayList<String> series1, ArrayList<String> series2, int burnin1, int burnin2, double alpha, boolean filteredCases) {
		String[] nwStrs1 = new String[series1.size() - burnin1];
		String[] nwStrs2 = new String[series2.size() - burnin2];
				
		for(int i = burnin1; i < series1.size(); i++) 
			nwStrs1[i - burnin1] = series1.get(i);
		for(int i = burnin2; i < series2.size(); i++) 
			nwStrs2[i - burnin2] = series2.get(i);
						
		//Scanning through first instance of the array
		ArrayList<String> leaves1 =  scannwStr(nwStrs1[0]);
		ArrayList<String> leaves2 =  scannwStr(nwStrs2[0]);
		
		assert( leaves1.size() == leaves2.size() );
		
		boolean same = true; 
		for(String l: leaves1) {
			if(!leaves2.contains(l))
				same = false;
		}
		
		if(!same) {
			JOptionPane.showMessageDialog(new JFrame(), "Error: leaves names are not same in either of the variable.");
			return null;
		}
		
		name2id = new HashMap<String, Integer>();
		id2name = new HashMap<Integer,String>() ;
		for(int i=0 ; i<leaves1.size() ; i++) {
			name2id.put(leaves1.get(i),i);
			id2name.put(i,leaves1.get(i));
		}
				
		//Preparing splitmap
		HashMap<List<Integer>,Integer> splitMap1,splitMap2;
		HashMap<List<Integer>,Integer> split1 = new HashMap<List<Integer>,Integer>();
		HashMap<List<Integer>,Integer> split2 = new HashMap<List<Integer>,Integer>();
		for(int i=0 ; i< nwStrs1.length ; i++ ) 
			parseNwStr(nwStrs1[i],  split1);
		for(int i=0 ; i< nwStrs2.length ; i++ ) 
			parseNwStr(nwStrs2[i],  split2);

		// Filtering out cases less than 5%
		if(filteredCases) {
			HashMap<List<Integer>,Integer> splitMap1_f = new HashMap<List<Integer>,Integer>();
			HashMap<List<Integer>,Integer> splitMap2_f = new HashMap<List<Integer>,Integer>();

			for(List<Integer> list: split1.keySet()) {
				int cases = split1.get(list);
				if( (double) cases/ (double) (series1.size() - burnin1) >= 0.05)
					splitMap1_f.put(list, cases);
			}

			for(List<Integer> list: split2.keySet()) {
				int cases = split2.get(list);
				if( (double) cases/ (double) (series2.size() - burnin2) >= 0.05)
					splitMap2_f.put(list, cases);
			}

			splitMap1 = splitMap1_f;
			splitMap2 = splitMap2_f;
		} else {
			splitMap1 = split1;
			splitMap2 = split2;

		}
		
		StringBuilder sb = new StringBuilder();
				
		ArrayList<Integer> sample1 = new ArrayList<Integer>();
		ArrayList<Integer> sample2 = new ArrayList<Integer>();
		
		for(List<Integer> list: splitMap1.keySet()) {
			if(splitMap2.containsKey(list)  ) {
				sample1.add(splitMap1.get(list));
				sample2.add(splitMap2.get(list));
				sb.append(String.format("%-50s", list.toString()) + ": " + "\t\t" + splitMap1.get(list) + "\t" + splitMap2.get(list) + "\n");
			} else {
				//sample1.add(splitMap1.get(list));
				//sample2.add(0);
//				sb.append(String.format("%-50s", list.toString())+ ": " + "\t\t" + splitMap1.get(list) + "\t" + 0 + "\n");
			}
		}
/*		for(List<Integer> list: splitMap2.keySet()) {
			if(!splitMap1.containsKey(list)  ) {
				sample1.add(0);
				sample2.add(splitMap2.get(list));
				sb.append(String.format("%-50s", list.toString()) + ": " + "\t\t" + 0 + "\t" + splitMap2.get(list) + "\n");
			}
		}
*/		
		assert(sample1.size() == sample2.size());
		
		long [] s1 = new long[sample1.size()];
		long [] s2 = new long[sample2.size()];
				
		for(int i=0 ; i<sample1.size() ; i++ ) {
			s1[i] = sample1.get(i).longValue();
			s2[i] = sample2.get(i).longValue();
		}
		
		//Applying test on data
		ChiSquareTest chi  = new ChiSquareTest();

		ArrayList<String> result = new ArrayList<String>();
		result.add(String.valueOf(chi.chiSquareDataSetsComparison(s1,s2)));
		result.add(String.valueOf(chi.chiSquareTestDataSetsComparison(s1,s2)));
		
		return result;
	}
			
	//This method will show the mapping of splits and their respective frequencies
	public static void show(HashMap<List<Integer>,Integer> splitMap) {
		System.out.println("----------------------------------------------");
		for (List<Integer> key : splitMap.keySet()) 
			System.out.println(key.toString() + " : " + splitMap.get(key));

	}
}

class Node {
	public int id;                      // Node id (matches index in tree.nodes)
	public String name;                 // Node name (used mainly for leaves only)
	public Node parent;                 // Parent of this node 
	public int nchildren;               // Number of children
	public ArrayList<Node> children;    // Array of child pointers (size = nchildren)
	//Default constructor
	public Node() {
		parent = null;
		nchildren = 0;
		id = -1;
		children = new ArrayList<Node>(); 
	}
	// Adds a child node
	public void addChild(Node node) {
		nchildren++;
		children.add(node);
		node.parent = this;
	}
}

