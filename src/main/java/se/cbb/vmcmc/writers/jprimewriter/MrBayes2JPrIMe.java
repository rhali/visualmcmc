package se.cbb.vmcmc.writers.jprimewriter;

import java.io.File;
import java.util.ArrayList;

import se.cbb.vmcmc.libs.MCMCDataContainer;
import se.cbb.vmcmc.libs.MCMCFileReader;
import se.cbb.vmcmc.libs.SequenceFileWriter;

public class MrBayes2JPrIMe {
	public static void main(String mrBayesFileName, String path, String jprimeFileName) {
		try {
			

			ArrayList<String> list = generateList(path, ".p");
			for (int k = 0; k < list.size(); k++) {
				
				File mrBayesFile = new File(path + list.get(k) + ".p"); 
				
				if(mrBayesFile != null) {
					System.out.println(mrBayesFile.getAbsolutePath());
					MCMCDataContainer datacontainer = null;
					try {
						datacontainer = MCMCFileReader.readMCMCFile(mrBayesFile, false, true);
					} catch (Exception e) {
						System.out.println("Error in reading file: " + e.getMessage());
						System.exit(-1);
					}
					
					System.out.println("Processing file : " + path + list.get(k) + ".p - Output to : " + path + list.get(k) + ".jprime.mcmc");
					int numLines = datacontainer.getNumLines();
					int numNumericSeries = datacontainer.getNumSeries() - 1;
					int numTreeSeries = datacontainer.getNumTreeSeries();

					SequenceFileWriter.writeAndAppendString(path, list.get(k) + ".jprime.mcmc", datacontainer.getValueNames().get(0) + "\t");
					SequenceFileWriter.writeAndAppendString(path, list.get(k) + ".jprime.mcmc", datacontainer.getValueNames().get(1) + "\t");
					SequenceFileWriter.writeAndAppendString(path, list.get(k) + ".jprime.mcmc", datacontainer.getValueNames().get(2) + "\t");
					SequenceFileWriter.writeAndAppendString(path, list.get(k) + ".jprime.mcmc", datacontainer.getValueNames().get(3) + "\t");
					SequenceFileWriter.writeAndAppendString(path, list.get(k) + ".jprime.mcmc", datacontainer.getValueNames().get(4) + "\t");
					SequenceFileWriter.writeAndAppendLine(path, list.get(k) + ".jprime.mcmc", datacontainer.getTreeNames().get(0));
					for(int i = 0; i < numLines; i++) {
						SequenceFileWriter.writeAndAppendString(path, list.get(k) + ".jprime.mcmc", String.valueOf(datacontainer.getValueSeries().get(0).get(i) + 100 * i));
						for(int j = 1; j < numNumericSeries - 1; j++) {
							SequenceFileWriter.writeAndAppendString(path, list.get(k) + ".jprime.mcmc", "\t" + datacontainer.getValueSeries().get(j).get(i));
						}

						for(int j = 0; j < numTreeSeries; j++) {
							SequenceFileWriter.writeAndAppendString(path, list.get(k) + ".jprime.mcmc", "\t" + new String(datacontainer.getTreeSeries().get(j).get(i).getData()));
						}
						SequenceFileWriter.writeAndAppendLine(path, list.get(k) + ".jprime.mcmc", "");
					} 
				}
			}
		} catch (Exception e) {
			System.out.println("Error in conversion: " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public static ArrayList<String> generateList(String path, String extension) {
		try {
			File folder = new File(path);
			File[] listOfFiles = folder.listFiles();
			int numberofFiles = listOfFiles.length;
			ArrayList<String> list = new ArrayList<String>();

			for(int i = 0; i < numberofFiles; i++) {
				if(listOfFiles[i].getName().endsWith(extension))
					list.add(listOfFiles[i].getName().substring(0, listOfFiles[i].getName().length()-extension.length()));
			}
			return list;
		} catch (Exception e) {
			System.out.println("Error in list finder : " + e.getMessage());
			System.exit(-1);
		}
		return null;
	}
}
