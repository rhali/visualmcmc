package se.cbb.vmcmc.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.net.URI;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import se.cbb.vmcmc.gui.MCMCWindow;
import se.cbb.vmcmc.libs.MCMCNumericFunctions;

public class MCMCAboutAndHelpItems extends Thread {
	public static void itemAboutMenu(MCMCWindow window) {
		JFrame frame = new JFrame();
		
		JPanel aboutPanel = new JPanel();
		JPanel namePanel = new JPanel();
		JPanel imagePanel = new JPanel(new GridLayout(3, 1));
		
		JLabel title = new JLabel(" VISUAL MCMC Version 1.0");
		JLabel title1 = new JLabel("");
		JLabel creation = new JLabel(" Created by: ");
		JLabel creation1 = new JLabel("Jorge Miró & Mikael Bark");
		JLabel developer = new JLabel(" Developed by: ");
		JLabel developer1 = new JLabel("Raja Hashim Ali");
		JLabel aidees = new JLabel(" Aided by: ");
		JLabel aidees1 = new JLabel("Zubair Syed, Raja Manzar Abbas and Sayyed Auwn Muhammad");
		JLabel supervisor = new JLabel(" Supervised by: ");
		JLabel supervisor1 = new JLabel("Lars Arvestad");
		JLabel designer = new JLabel(" Designed by: ");
		JLabel designer1 = new JLabel("Raja Hashim Ali & Lars Arvestad");
		JLabel properieter = new JLabel(" Affiliations : ");
		JLabel properieter1 = new JLabel("SciLifeLab Stockholm & Kungliga Tekniska Högskolan (KTH) Sweden");
		JLabel feedback = new JLabel(" Comments, Feedback & Suggestions: ");
		JLabel feedback1 = new JLabel("rhali at kth dot se & arve at csc dot kth dot se");
		JLabel web = new JLabel(" Tutorials and source code : ");
		JLabel web1 = new JLabel("https://bitbucket.org/rhali/visualmcmc/");
		
		Font font0 = new Font("SansSerif", Font.BOLD, 15);
		Font font1 = new Font("SansSerif", Font.ITALIC, 12);
		Font font2 = new Font("SansSerif", Font.BOLD, 12);
	
		aboutPanel.setLayout(new GridLayout(12, 1));
		namePanel.setLayout(new GridLayout(12, 1));
		
		addToPanel(title, font0, Color.BLUE, aboutPanel);
		addToPanel(title1, font0, Color.LIGHT_GRAY, namePanel);
        
		aboutPanel.add(new JSeparator()); 
		namePanel.add(new JSeparator());
		
		addToPanel(creation, font1, Color.BLACK, aboutPanel);
		addToPanel(creation1, font2, Color.DARK_GRAY, namePanel);
		addToPanel(developer, font1, Color.BLACK, aboutPanel);
		addToPanel(developer1, font2, Color.DARK_GRAY, namePanel);
		addToPanel(aidees, font1, Color.BLACK, aboutPanel);
		addToPanel(aidees1, font2, Color.DARK_GRAY, namePanel);
		addToPanel(supervisor, font1, Color.BLACK, aboutPanel);
		addToPanel(supervisor1, font2, Color.DARK_GRAY, namePanel);
		addToPanel(designer, font1, Color.BLACK, aboutPanel);
		addToPanel(designer1, font2, Color.DARK_GRAY, namePanel);		
		addToPanel(properieter, font1, Color.BLACK, aboutPanel);
		addToPanel(properieter1, font2, Color.DARK_GRAY, namePanel);
		
		aboutPanel.add(new JSeparator());
		namePanel.add(new JSeparator());
		
		addToPanel(feedback, font1, Color.BLACK, aboutPanel);
		addToPanel(feedback1, font2, Color.DARK_GRAY, namePanel);
		addToPanel(web, font1, Color.BLACK, aboutPanel);
		addToPanel(web1, font2, Color.DARK_GRAY, namePanel);
		
		aboutPanel.setBackground(new Color(0xFFDDDDFF));
		namePanel.setBackground(new Color(0xFFDDDDFF));
																		
		frame.setTitle("Team Visual Markov Chain Monte Carlo");
        
		imagePanel.add(new JLabel(new ImageIcon(window.getIconImage()))); 
    	MCMCNumericFunctions.readImageFiles(imagePanel, window, "/a.png");
    	MCMCNumericFunctions.readImageFiles(imagePanel, window, "/b.png");
						
        imagePanel.setBackground(Color.BLUE);
        imagePanel.setSize(new Dimension(100, 230));
		frame.add(imagePanel,BorderLayout.WEST);
		frame.add(aboutPanel, BorderLayout.CENTER);
		frame.add(namePanel, BorderLayout.EAST);
		
		frame.setAlwaysOnTop(true);
		frame.setSize(600, 230);
		frame.setBackground(Color.BLUE);
		frame.pack();
		frame.setResizable(true);
		frame.setVisible(true);
		frame.setFocusable(false);
	}
	
	public static void addToPanel(JLabel title, Font font0, Color color, JPanel aboutPanel) {
		title.setFont(font0);
		title.setForeground(color);
		aboutPanel.add(title);
	}
	
	public static void openTutorial(MCMCWindow window) {
		URI url;
		try {
			url = new URI("https://drive.google.com/file/d/0Bxs0shuzgCXsTlMwTUQ0ZW8tNlE/view?usp=sharing");
			java.awt.Desktop.getDesktop().browse(url);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(new JFrame(), "Warning: Unable to open the webpage.");
		}
	}
}
