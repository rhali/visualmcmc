package se.cbb.vmcmc;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.commons.lang3.text.WordUtils;

import se.cbb.vmcmc.computations.TrueTreePresence;
import se.cbb.vmcmc.gui.MCMCApplication;
import se.cbb.vmcmc.main.MCMCCommandLine;
import se.cbb.vmcmc.libs.ParameterParser;
import se.cbb.vmcmc.libs.Parameters;
import se.cbb.vmcmc.libs.Triple;
import se.cbb.vmcmc.writers.jprimewriter.MrBayes2JPrIMe;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterDescription;

public class VMCMCStarter {

	public String getAppName() {
		return "VMCMC";
	}

	/** Definition: 			Main function for VMCMC.										
	<p>Usage: 				Initialize the application from command line.						
 	<p>Function:			Gets the inputs from command line, parses them and calls the appropriate constructor of MCMCWindow. 				
 	<p>Classes:				Parameters, JCommander, JCommanderUserWrapper, Triple.
 	<p>Internal Functions: 	MCMCApplication(),  		
 	@return 				(A new graphical window)/(command line) statistical and/or convergence test analysis.					
	 */
	public static void main(String[] args) {
		/* Function Variables and Initialization */
		Parameters params = new Parameters();
		JCommander commander;
		
		try {
			commander = new JCommander(params, args);

			/* Print the help menu */
			if (params.help) {
				printHelpAndUsage(params, commander);
				return;
			}
		}
		catch (Exception e) {
			System.err.println("Error! " + e.getMessage());
			System.exit(-1);
		}

		try {
			if (args.length == 0)                           
				new MCMCApplication();
			else if ((args.length == 1) && (params.nogui == false) && (params.test == false) && (params.stats == false) && (params.ess == false) && (params.geweke == false) && (params.gr == false) && (params.posterior == false) && (params.convergencetest == false) && (params.maptree == false) && (params.sampledata == false) && (params.trueTest == false) && (params.treeconvergence == false)) {
			 		// Run the VMCMC GUI directly with the specified file as input. Useful when analyzing file in GUI directly from command line.
					new MCMCApplication(params.files.get(0));
			}
			else {
				String path = params.path;
				if(!path.endsWith("/"))
					path = path + "/";

				if(!params.paramfile.equals(""))
					params = new Parameters(); // Does this actually do something? Isn't the params.paramfile needed somehow? /arve

				runVMCMCUsingCommandLineParams(params, path, Double.parseDouble(params.alpha), Integer.parseInt(params.burnin1), Integer.parseInt(params.burnin2));
			}
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
			System.exit(-1);
		}
	}

	private static void printHelpAndUsage(Parameters params,
			JCommander commander) {
		StringBuilder sb = new StringBuilder(65536);
		sb.append("USAGE:\n\tjava [-Xmx2000m -Xms1800m] -jar VMCMC-x.y.z.jar").append('\n');
		sb.append("  Single Chain:   \n\tjava [-Xmx2000m -Xms1800m] -jar VMCMC-x.y.z.jar [options] <filename> ").append('\n');
		sb.append("  Parallel Chains:\n\tjava [-Xmx2000m -Xms1800m] -jar VMCMC-x.y.z.jar [options] <filename1> <filename2> ").append('\n');
		

		ParameterDescription mainParam = commander.getMainParameter();
		if (mainParam != null) {
			sb.append("\nOPTIONS:\n");
			sb.append("     ").append(mainParam.getDescription()).append('\n');
		}
		List<ParameterDescription> params1 = commander.getParameters();
		Field[] fields = params.getClass().getFields();
		for (Field f : fields) {
			for (ParameterDescription p : params1) {
				if (f.getName().equals(p.getField().getName())) {
					sb.append(p.getNames()).append('\n');
					String def = (p.getDefault() == null ? "" : " Default: " + p.getDefault().toString() + '.');
					String desc = WordUtils.wrap(p.getDescription() + def, 120);
					desc = "     " + desc;
					desc = desc.replaceAll("\n", "\n     ") + '\n';
					sb.append(desc);
					break;
				}
			}
		}
		System.out.println(sb.toString());
	}

	private static void runVMCMCUsingCommandLineParams(Parameters params, String path, Double alpha, int burnin1, int burnin2) throws Exception {
		// Filename not provided. All functions expect a file to work with.
		if (params.files.size() < 1) {
			throw new Exception("No filename given. When starting VMCMC on the commandline, an input file is always needed.");
		} 

		// Convert a MrBayes MCMC file with .p extension and (optionally a .t file) to its tab separated jprime formatted file.
		else if (params.convertMrBayes2jprime == true) 
			MrBayes2JPrIMe.main(params.files.get(0), path, params.outFile);

		// GUI version of parallel chain analysis for two chains run in parallel. Runs when two filenames are provided.
		else if(params.files.size() == 2) 
			MCMCCommandLine.commandline(0, params.files.get(0), params.files.get(1), -1, 0.95, path, params.outFile, alpha, burnin1, burnin2);

		// Compute the posterior of MCMC chain for the tree parameter (without branch lengths or after removal of branch lengths) in the MCMC run.
		else if(params.posterior == true) {
			Triple<String, Integer, Double> paramData = ParameterParser.getOptions(params);
			MCMCCommandLine.commandline(10, paramData.first, "", paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);			
			MCMCCommandLine.printLine(path, params.outFile, "\t}\n}");
		} 

		// Determine the maximum a posteriori (MAP) tree for the tree (without branch length) for the tree parameter in the MCMC run.
		else if (params.maptree == true) {
			Triple<String, Integer, Double> paramData = ParameterParser.getOptions(params);
			MCMCCommandLine.commandline(11, paramData.first, "", paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);
			MCMCCommandLine.printLine(path, params.outFile, "}");
		} 

		// Determine the maximum a posteriori (MAP) tree for the tree (without branch length) for the tree parameter in the MCMC run.
		else if (params.treeconvergence == true) {
			Triple<String, Integer, Double> paramData = ParameterParser.getOptions(params);
			MCMCCommandLine.commandline(12, paramData.first, "", paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);
			MCMCCommandLine.printLine(path, params.outFile, "}");
		} 

		// Compute all parameter statistics and convergence tests estimates and display in the command line.
		else if (params.nogui == true) {
			Triple<String, Integer, Double> paramData = ParameterParser.getOptions(params);
			MCMCCommandLine.commandline(7, paramData.first, "", paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);
			MCMCCommandLine.printLine(path, params.outFile, "\n\t}\n}");
		} 

		// Analysis estimates for VMCMC 2. To be removed on completion of convergence estimate analysis from the final version of VMCMC.
		else if (params.trueTest == true) 
			TrueTreePresence.generateScriptForPosterior(path, "essstandardized");

		//Command line results for various analysis.
		else {
			Triple<String, Integer, Double> paramData = ParameterParser.getOptions(params);
			// Compute the results of convergence tests only.
			if(params.convergencetest == true) 
				MCMCCommandLine.commandline(8, paramData.first,"",  paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);

			// Pick one sample data after removing burnin (estimated using ESS-last).
			else if (params.sampledata == true) {
				MCMCCommandLine.commandline(9, paramData.first, "", paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);
			}
			// Compute the results of all convergence tests only.
			else if (params.test == true)
				MCMCCommandLine.commandline(2, paramData.first, "", paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);

			// Compute the parameter statistics only.
			else if (params.stats == true)
				MCMCCommandLine.commandline(3, paramData.first, "", paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);

			// Estimate burn-in values for all parameters using Geweke.
			else if (params.geweke == true)
				MCMCCommandLine.commandline(4, paramData.first, "", paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);

			// Estimate burn-in values for all parameters using ESS.
			else if (params.ess == true)
				MCMCCommandLine.commandline(5, paramData.first, "", paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);

			// EStimate burn-in values for all parameters using Gelman Rubin.
			else if (params.gr == true)
				MCMCCommandLine.commandline(6, paramData.first, "", paramData.second, paramData.third, path, params.outFile, alpha, burnin1, burnin2);
			
			MCMCCommandLine.printLine(path, params.outFile, "\n\n}"); 
		}
	}
}
