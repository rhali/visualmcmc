
package org.forester1.io.parsers;

import java.io.IOException;

import org.forester1.phylogeny.Phylogeny;

public interface IteratingPhylogenyParser {

    public void reset() throws IOException;

    public Phylogeny next() throws IOException;

    public boolean hasNext();

    public void setSource( final Object o ) throws IOException;
}
