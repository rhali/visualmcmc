package org.forester1.archaeopteryx;

import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/*
 * @author Christian Zmasek
 */
final class MouseListener extends MouseAdapter implements MouseMotionListener {

    private final TreePanel _treepanel;
    private boolean         _being_dragged = false;
    private final Point     _click_point   = new Point();

    /**
     * Constructor.
     */
    MouseListener( final TreePanel tp ) {
        _treepanel = tp;
    }

    /**
     * Mouse clicked.
     */
    @Override
    public void mouseClicked( final MouseEvent e ) {
        _click_point.setLocation( e.getX(), e.getY() );
        _treepanel.mouseClicked( e );
    }

    @Override
    public void mouseDragged( final MouseEvent e ) {
        if ( ( e.getModifiersEx() == InputEvent.BUTTON1_DOWN_MASK )
                || ( e.getModifiersEx() == InputEvent.BUTTON3_DOWN_MASK ) ) {
            if ( !_treepanel.inOvRectangle( e ) ) {
                if ( !_being_dragged ) {
                    _being_dragged = true;
                    _treepanel.setLastMouseDragPointX( e.getX() );
                    _treepanel.setLastMouseDragPointY( e.getY() );
                }
                _treepanel.mouseDragInBrowserPanel( e );
            }
            else {
                if ( !_being_dragged ) {
                    _being_dragged = true;
                    _treepanel.setLastMouseDragPointX( e.getX() );
                    _treepanel.setLastMouseDragPointY( e.getY() );
                }
                _treepanel.mouseDragInOvRectangle( e );
            }
        }
    }

    @Override
    public void mouseMoved( final MouseEvent e ) {
        _treepanel.mouseMoved( e );
    }

    @Override
    public void mousePressed( final MouseEvent e ) {
        //TODO is this a good idea? It is certainly not NEEDED.
        if ( e.getModifiersEx() == InputEvent.BUTTON1_DOWN_MASK ) {
            if ( !_being_dragged ) {
                _being_dragged = true;
                _treepanel.setLastMouseDragPointX( e.getX() );
                _treepanel.setLastMouseDragPointY( e.getY() );
            }
            if ( !_treepanel.inOvRectangle( e ) ) {
                _treepanel.mouseDragInBrowserPanel( e );
            }
            else {
                _treepanel.mouseDragInOvRectangle( e );
            }
        }
    }

    @Override
    public void mouseReleased( final MouseEvent e ) {
        if ( _being_dragged ) {
            _being_dragged = false;
        }
        _treepanel.mouseReleasedInBrowserPanel( e );
    }
}