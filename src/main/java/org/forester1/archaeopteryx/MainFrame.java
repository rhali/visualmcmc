package org.forester1.archaeopteryx;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import org.forester1.archaeopteryx.tools.ProcessPool;
import org.forester1.archaeopteryx.tools.ProcessRunning;

public abstract class MainFrame extends JFrame implements ActionListener {

    static final String         USE_MOUSEWHEEL_SHIFT_TO_ROTATE          = "In this display type, use mousewheel + Shift to rotate [or A and S]";
    static final String         PHYLOXML_REF_TOOL_TIP                   = Constants.PHYLOXML_REFERENCE;                                                                                                                                       //TODO //FIXME
    static final String         APTX_REF_TOOL_TIP                       = Constants.APTX_REFERENCE;
    private static final long   serialVersionUID                        = 3655000897845508358L;
    final static Font           menu_font                               = new Font( Configuration.getDefaultFontFamilyName(),
                                                                                    Font.PLAIN,
                                                                                    10 );
    // view as text menu:
    JMenu                       _process_menu;
    // Handy pointers to child components:
    MainPanel                   _mainpanel;
    Container                   _contentpane;
    Configuration               _configuration;
    Options                     _options;
    final ProcessPool           _process_pool;

    public MainFrame() {
        _process_pool = ProcessPool.createInstance();
    }

    /**
     * Action performed.
     */
    @Override
    public void actionPerformed( final ActionEvent e ) {
        _contentpane.repaint();
    }

    public Configuration getConfiguration() {
        return _configuration;
    }

    public MainPanel getMainPanel() {
        return _mainpanel;
    }

    public Options getOptions() {
        return _options;
    }

    public ProcessPool getProcessPool() {
        return _process_pool;
    }

    public void updateProcessMenu() {
        // In general Swing is not thread safe.
        // See "Swing's Threading Policy".
        SwingUtilities.invokeLater( new Runnable() {

            @Override
            public void run() {
                doUpdateProcessMenu();
            }
        } );
    }

    JMenuItem customizeJMenuItem( final JMenuItem jmi ) {
        if ( jmi != null ) {
            jmi.setFont( MainFrame.menu_font );
            if ( !getConfiguration().isUseNativeUI() ) {
                jmi.setBackground( getConfiguration().getGuiMenuBackgroundColor() );
                jmi.setForeground( getConfiguration().getGuiMenuTextColor() );
            }
            jmi.addActionListener( this );
        }
        return jmi;
    }

    void setConfiguration( final Configuration configuration ) {
        _configuration = configuration;
    }

    void setOptions( final Options options ) {
        _options = options;
    }

    private void doUpdateProcessMenu() {
        if ( _process_pool.size() > 0 ) {
            if ( _process_menu == null ) {
                _process_menu = createMenu( "", getConfiguration() );
                _process_menu.setForeground( Color.RED );
            }
            _process_menu.removeAll();
            final String text = "processes running: " + _process_pool.size();
            _process_menu.setText( text );
            for( int i = 0; i < _process_pool.size(); ++i ) {
                final ProcessRunning p = _process_pool.getProcessByIndex( i );
                _process_menu.add( customizeJMenuItem( new JMenuItem( p.getName() + " [" + p.getStart() + "]" ) ) );
            }
        }
        repaint();
    }

    static JMenu createMenu( final String title, final Configuration conf ) {
        final JMenu jmenu = new JMenu( title );
        if ( !conf.isUseNativeUI() ) {
            jmenu.setFont( MainFrame.menu_font );
            jmenu.setBackground( conf.getGuiMenuBackgroundColor() );
            jmenu.setForeground( conf.getGuiMenuTextColor() );
        }
        return jmenu;
    }

    static void cycleOverview( final Options op, final TreePanel tree_panel ) {
        switch ( op.getOvPlacement() ) {
            case LOWER_LEFT:
                op.setOvPlacement( Options.OVERVIEW_PLACEMENT_TYPE.UPPER_LEFT );
                break;
            case LOWER_RIGHT:
                op.setOvPlacement( Options.OVERVIEW_PLACEMENT_TYPE.LOWER_LEFT );
                break;
            case UPPER_LEFT:
                op.setOvPlacement( Options.OVERVIEW_PLACEMENT_TYPE.UPPER_RIGHT );
                break;
            case UPPER_RIGHT:
                op.setOvPlacement( Options.OVERVIEW_PLACEMENT_TYPE.LOWER_RIGHT );
                break;
            default:
                throw new RuntimeException( "unknown placement: " + op.getOvPlacement() );
        }
        if ( tree_panel != null ) {
            tree_panel.updateOvSettings();
        }
    }
}