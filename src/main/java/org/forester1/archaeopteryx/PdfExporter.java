package org.forester1.archaeopteryx;

import java.awt.Graphics2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.forester1.util.ForesterUtil;

import se.cbb.jprime.consensus.day.MDSPanel;
import se.cbb.vmcmc.gui.MCMCWindow;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.DefaultFontMapper;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

public final class PdfExporter {

	private static final int HEIGHT_LIMIT = 100;
	private static final int WIDTH_LIMIT  = 60;

	private PdfExporter() {
		// Empty constructor.
	}

	public static String writeToPdf( final String file_name, final MCMCWindow tree_panel, int width, int height )
	throws IOException {
		if ( height < HEIGHT_LIMIT ) {
			height = HEIGHT_LIMIT;
		}
		if ( width < WIDTH_LIMIT ) {
			width = WIDTH_LIMIT;
		}
		final File file = new File( file_name );
		if ( file.isDirectory() ) {
			throw new IOException( "[" + file_name + "] is a directory" );
		}
		final Document document = new Document();
		document.setPageSize( new Rectangle( width, height ) );
		document.setMargins( WIDTH_LIMIT / 2, WIDTH_LIMIT / 2, HEIGHT_LIMIT / 2, HEIGHT_LIMIT / 2 );
		PdfWriter writer = null;
		try {
			writer = PdfWriter.getInstance( document, new FileOutputStream( file_name ) );
		}
		catch ( final DocumentException e ) {
			throw new IOException( e );
		}
		document.open();
		final DefaultFontMapper mapper = new DefaultFontMapper();
		FontFactory.registerDirectories();
		if ( ForesterUtil.isWindows() ) {
			mapper.insertDirectory( "C:\\WINDOWS\\Fonts\\" );
		}
		else if ( ForesterUtil.isMac() ) {
			mapper.insertDirectory( "/Library/Fonts/" );
			mapper.insertDirectory( "/System/Library/Fonts/" );
		}
		else {
			mapper.insertDirectory( "/usr/X/lib/X11/fonts/TrueType/" );
			mapper.insertDirectory( "/usr/X/lib/X11/fonts/Type1/" );
			mapper.insertDirectory( "/usr/share/fonts/default/TrueType/" );
			mapper.insertDirectory( "/usr/share/fonts/default/Type1/" );
		}
		final PdfContentByte cb = writer.getDirectContent();
		final Graphics2D g2 = cb.createGraphics( width, height, mapper );
		tree_panel.paint(g2);
		try {
			g2.dispose();
			document.close();
		}
		catch ( final Exception e ) {
			//Do nothing.
		}
		String msg = file.toString();
		if ( ( width > 0 ) && ( height > 0 ) ) {
			msg += " [size: " + width + ", " + height + "]";
		}
		return msg;
	}

	public static String writeToPdf( final String file_name, final MainPanel tree_panel, int width, int height )
	throws IOException {
		if ( height < HEIGHT_LIMIT ) {
			height = HEIGHT_LIMIT;
		}
		if ( width < WIDTH_LIMIT ) {
			width = WIDTH_LIMIT;
		}
		final File file = new File( file_name );
		if ( file.isDirectory() ) {
			throw new IOException( "[" + file_name + "] is a directory" );
		}
		final Document document = new Document();
		document.setPageSize( new Rectangle( width, height ) );
		document.setMargins( WIDTH_LIMIT / 2, WIDTH_LIMIT / 2, HEIGHT_LIMIT / 2, HEIGHT_LIMIT / 2 );
		PdfWriter writer = null;
		try {
			writer = PdfWriter.getInstance( document, new FileOutputStream( file_name ) );
		}
		catch ( final DocumentException e ) {
			throw new IOException( e );
		}
		document.open();
		final DefaultFontMapper mapper = new DefaultFontMapper();
		FontFactory.registerDirectories();
		if ( ForesterUtil.isWindows() ) {
			mapper.insertDirectory( "C:\\WINDOWS\\Fonts\\" );
		}
		else if ( ForesterUtil.isMac() ) {
			mapper.insertDirectory( "/Library/Fonts/" );
			mapper.insertDirectory( "/System/Library/Fonts/" );
		}
		else {
			mapper.insertDirectory( "/usr/X/lib/X11/fonts/TrueType/" );
			mapper.insertDirectory( "/usr/X/lib/X11/fonts/Type1/" );
			mapper.insertDirectory( "/usr/share/fonts/default/TrueType/" );
			mapper.insertDirectory( "/usr/share/fonts/default/Type1/" );
		}
		final PdfContentByte cb = writer.getDirectContent();
		final Graphics2D g2 = cb.createGraphics( width, height, mapper );
		tree_panel.paint(g2);
		try {
			g2.dispose();
			document.close();
		}
		catch ( final Exception e ) {
			//Do nothing.
		}
		String msg = file.toString();
		if ( ( width > 0 ) && ( height > 0 ) ) {
			msg += " [size: " + width + ", " + height + "]";
		}
		return msg;
	}
	
	public static String writeToPdf(final String file_name, final MDSPanel tree_panel, int width, int height )
	throws IOException {
		if ( height < HEIGHT_LIMIT ) {
			height = HEIGHT_LIMIT;
		}
		if ( width < WIDTH_LIMIT ) {
			width = WIDTH_LIMIT;
		}
		final File file = new File( file_name );
		if ( file.isDirectory() ) {
			throw new IOException( "[" + file_name + "] is a directory" );
		}
		final Document document = new Document();
		document.setPageSize( new Rectangle( width, height ) );
		document.setMargins( WIDTH_LIMIT / 2, WIDTH_LIMIT / 2, HEIGHT_LIMIT / 2, HEIGHT_LIMIT / 2 );
		PdfWriter writer = null;
		try {
			writer = PdfWriter.getInstance( document, new FileOutputStream( file_name ) );
		}
		catch ( final DocumentException e ) {
			throw new IOException( e );
		}
		document.open();
		final DefaultFontMapper mapper = new DefaultFontMapper();
		FontFactory.registerDirectories();
		if ( ForesterUtil.isWindows() ) {
			mapper.insertDirectory( "C:\\WINDOWS\\Fonts\\" );
		}
		else if ( ForesterUtil.isMac() ) {
			mapper.insertDirectory( "/Library/Fonts/" );
			mapper.insertDirectory( "/System/Library/Fonts/" );
		}
		else {
			mapper.insertDirectory( "/usr/X/lib/X11/fonts/TrueType/" );
			mapper.insertDirectory( "/usr/X/lib/X11/fonts/Type1/" );
			mapper.insertDirectory( "/usr/share/fonts/default/TrueType/" );
			mapper.insertDirectory( "/usr/share/fonts/default/Type1/" );
		}
		final PdfContentByte cb = writer.getDirectContent();
		final Graphics2D g2 = cb.createGraphics( width, height, mapper );
		tree_panel.paint(g2);
		try {
			g2.dispose();
			document.close();
		}
		catch ( final Exception e ) {
			//Do nothing.
		}
		String msg = file.toString();
		if ( ( width > 0 ) && ( height > 0 ) ) {
			msg += " [size: " + width + ", " + height + "]";
		}
		return msg;
	}
}
