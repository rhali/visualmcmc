package org.forester1.archaeopteryx;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.forester1.phylogeny.Phylogeny;

public class MainPanel extends JPanel implements ComponentListener {

    private static final long                serialVersionUID = -2682765312661416435L;
//    MainFrame                                _mainframe;
    List<TreePanel>                          _treepanels;
    protected ControlPanel                   _control_panel;
    private List<JScrollPane>                _treegraphic_scroll_panes;
    private List<JPanel>                     _treegraphic_scroll_pane_panels;
    Configuration                            _configuration;
    private JTabbedPane                      _tabbed_pane;
    private TreeColorSet                     _colorset;
    private TreeFontSet                      _fontset;
    private Phylogeny                        _cut_or_copied_tree;
    private Set<Long>                        _copied_and_pasted_nodes;
    private Hashtable<String, BufferedImage> _image_map;
    private Options							 _options;

    public MainPanel() {
    }

    public MainPanel( final Configuration configuration) {
        if ( configuration == null ) {
            throw new IllegalArgumentException( "configuration is null" );
        }
        addComponentListener( this );
        _configuration = configuration;
        _options = Options.createInstance( _configuration );
        _treepanels = new ArrayList<TreePanel>();
        initialize();
        _control_panel = new ControlPanel( this, configuration );
        getControlPanel().showWhole();
    }

    public void addPhylogenyInNewTab( final Phylogeny phy,
                                      final Configuration config, 
                                      final String name,
                                      final int myX,
                                      final int myY) {
        final TreePanel treepanel = new TreePanel( phy, config, this );
        getControlPanel().phylogenyAdded( config );
        treepanel.setControlPanel( getControlPanel() );
        _treepanels.add( treepanel );
        final JScrollPane treegraphic_scroll_pane = new JScrollPane( treepanel );
        treegraphic_scroll_pane.getHorizontalScrollBar().addAdjustmentListener( new AdjustmentListener() {

            @Override
            public void adjustmentValueChanged( final AdjustmentEvent e ) {
                if ( treepanel.isOvOn() || getOptions().isShowScale() ) {
                    treepanel.repaint();
                }
            }
        } );
        treegraphic_scroll_pane.getVerticalScrollBar().addAdjustmentListener( new AdjustmentListener() {

            @Override
            public void adjustmentValueChanged( final AdjustmentEvent e ) {
                if ( treepanel.isOvOn() || getOptions().isShowScale() ) {
                    treepanel.repaint();
                    //System.out.println( e.getValue() );
                }
            }
        } );
        treegraphic_scroll_pane.getHorizontalScrollBar().setUnitIncrement( 10 );
        treegraphic_scroll_pane.getHorizontalScrollBar().setBlockIncrement( 200 );
        treegraphic_scroll_pane.getVerticalScrollBar().setUnitIncrement( 10 );
        treegraphic_scroll_pane.getVerticalScrollBar().setBlockIncrement( 200 );
        final JPanel treegraphic_scroll_pane_panel = new JPanel();
        treegraphic_scroll_pane_panel.setLayout( new BorderLayout() );
        treegraphic_scroll_pane_panel.add( treegraphic_scroll_pane, BorderLayout.CENTER );
        _treegraphic_scroll_pane_panels.add( treegraphic_scroll_pane_panel );
        _treegraphic_scroll_panes.add( treegraphic_scroll_pane );
        getTabbedPane().addTab( name, null, treegraphic_scroll_pane_panel, "" );
        JLabel lab = new JLabel(name);
        lab.setHorizontalAlignment( SwingConstants.CENTER );
        lab.setPreferredSize(new Dimension(myX, myY));
        _tabbed_pane.setTabComponentAt(0, lab);
        getTabbedPane().setSelectedIndex( getTabbedPane().getTabCount() - 1 );
        getControlPanel().showWhole();
    }

    void addPhylogenyInPanel( final Phylogeny phy, final Configuration config ) {
        final TreePanel treepanel = new TreePanel( phy, config, this );
        getControlPanel().phylogenyAdded( config );
        treepanel.setControlPanel( getControlPanel() );
        _treepanels.add( treepanel );
        final JScrollPane treegraphic_scroll_pane = new JScrollPane( treepanel );
        treegraphic_scroll_pane.getHorizontalScrollBar().addAdjustmentListener( new AdjustmentListener() {

            @Override
            public void adjustmentValueChanged( final AdjustmentEvent e ) {
                if ( treepanel.isOvOn() || getOptions().isShowScale() ) {
                    treepanel.repaint();
                }
            }
        } );
        treegraphic_scroll_pane.getVerticalScrollBar().addAdjustmentListener( new AdjustmentListener() {

            @Override
            public void adjustmentValueChanged( final AdjustmentEvent e ) {
                if ( treepanel.isOvOn() || getOptions().isShowScale() ) {
                    treepanel.repaint();
                    //System.out.println( e.getValue() );
                }
            }
        } );
        treegraphic_scroll_pane.getHorizontalScrollBar().setUnitIncrement( 20 );
        treegraphic_scroll_pane.getHorizontalScrollBar().setBlockIncrement( 50 );
        treegraphic_scroll_pane.getVerticalScrollBar().setUnitIncrement( 20 );
        treegraphic_scroll_pane.getVerticalScrollBar().setBlockIncrement( 50 );
        final JPanel treegraphic_scroll_pane_panel = new JPanel();
        treegraphic_scroll_pane_panel.setLayout( new BorderLayout() );
        treegraphic_scroll_pane_panel.add( treegraphic_scroll_pane, BorderLayout.CENTER );
        _treegraphic_scroll_pane_panels.add( treegraphic_scroll_pane_panel );
        _treegraphic_scroll_panes.add( treegraphic_scroll_pane );
        add( treegraphic_scroll_pane_panel, BorderLayout.CENTER );
    }

    void adjustJScrollPane() {
        if ( getTabbedPane() != null ) {
            getCurrentScrollPanePanel().remove( getCurrentScrollPane() );
            getCurrentScrollPanePanel().add( getCurrentScrollPane(), BorderLayout.CENTER );
        }
        getCurrentScrollPane().revalidate();
    }

    @Override
    public void componentHidden( final ComponentEvent e ) {
        // Do nothing.
    }

    @Override
    public void componentMoved( final ComponentEvent e ) {
        // Do nothing.
    }

    @Override
    public void componentResized( final ComponentEvent e ) {
        if ( getCurrentTreePanel() != null ) {
            getCurrentTreePanel().updateOvSettings();
            getCurrentTreePanel().updateOvSizes();
        }
    }

    @Override
    public void componentShown( final ComponentEvent e ) {
        // Do nothing.
    }

    protected Configuration getConfiguration() {
        return _configuration;
    }

    public ControlPanel getControlPanel() {
        return _control_panel;
    }

    public Set<Long> getCopiedAndPastedNodes() {
        return _copied_and_pasted_nodes;
    }

    Phylogeny getCurrentPhylogeny() {
        if ( getCurrentTreePanel() == null ) {
            return null;
        }
        return getCurrentTreePanel().getPhylogeny();
    }

    JScrollPane getCurrentScrollPane() {
        if ( _treegraphic_scroll_panes.size() > 0 ) {
            final int selected = getTabbedPane().getSelectedIndex();
            if ( selected >= 0 ) {
                return _treegraphic_scroll_panes.get( selected );
            }
            else {
                return _treegraphic_scroll_panes.get( 0 );
            }
        }
        else {
            return null;
        }
    }

    JPanel getCurrentScrollPanePanel() {
        final int selected = getTabbedPane().getSelectedIndex();
        if ( selected >= 0 ) {
            return _treegraphic_scroll_pane_panels.get( selected );
        }
        else {
            return _treegraphic_scroll_pane_panels.get( 0 );
        }
    }

    int getCurrentTabIndex() {
        final int selected = getTabbedPane().getSelectedIndex();
        if ( selected >= 0 ) {
            return selected;
        }
        else {
            return 0;
        }
    }

    void setTitleOfSelectedTab( final String title ) {
        final int selected = getTabbedPane().getSelectedIndex();
        if ( selected >= 0 ) {
            getTabbedPane().setTitleAt( selected, title );
        }
    }

    public TreePanel getCurrentTreePanel() {
        final int selected = getTabbedPane().getSelectedIndex();
        if ( selected >= 0 ) {
            return _treepanels.get( selected );
        }
        else {
            if ( _treepanels.size() == 1 ) {
                return _treepanels.get( 0 );
            }
            else {
                return null;
            }
        }
    }

    Phylogeny getCutOrCopiedTree() {
        return _cut_or_copied_tree;
    }

    public Options getOptions() {
        return _options;
    }

    Dimension getSizeOfViewport() {
        return getCurrentScrollPane().getViewport().getExtentSize();
    }

    public JTabbedPane getTabbedPane() {
        return _tabbed_pane;
    }

    TreeColorSet getTreeColorSet() {
        return _colorset;
    }

    public TreeFontSet getTreeFontSet() {
        return _fontset;
    }

    List<TreePanel> getTreePanels() {
        return _treepanels;
    }

    void initialize() {
        if ( !getConfiguration().isUseNativeUI() ) {
            setBackground( getConfiguration().getGuiBackgroundColor() );
        }
        setTreeFontSet( new TreeFontSet( this ) );
        getTreeFontSet().setBaseFont( getOptions().getBaseFont() );
        setLayout( new BorderLayout() );
        setTreeColorSet( TreeColorSet.createInstance( getConfiguration() ) );
        _treegraphic_scroll_panes = new ArrayList<JScrollPane>();
        _treegraphic_scroll_pane_panels = new ArrayList<JPanel>();
        _tabbed_pane = new JTabbedPane( SwingConstants.TOP );
        if ( !getConfiguration().isUseNativeUI() ) {
            _tabbed_pane.setBackground( getConfiguration().getGuiBackgroundColor() );
            _tabbed_pane.setForeground( getConfiguration().getGuiBackgroundColor() );
        }
        _tabbed_pane.addChangeListener( new ChangeListener() {

            // This method is called whenever the selected tab changes
            @Override
            public void stateChanged( final ChangeEvent evt ) {
                final JTabbedPane pane = ( JTabbedPane ) evt.getSource();
                getControlPanel().tabChanged();
                // Get current tab
                final int sel = pane.getSelectedIndex();
                if ( sel >= 0 ) {
                    if ( !getConfiguration().isUseNativeUI() ) {
                        if ( _tabbed_pane.getTabCount() > 0 ) {
                            _tabbed_pane.setForegroundAt( sel, Constants.TAB_LABEL_FOREGROUND_COLOR_SELECTED );
                            for( int i = 0; i < _tabbed_pane.getTabCount(); ++i ) {
                                if ( i != sel ) {
                                    _tabbed_pane.setBackgroundAt( i, getConfiguration().getGuiBackgroundColor() );
                                    _tabbed_pane.setForegroundAt( i, getConfiguration().getGuiCheckboxTextColor() );
                                }
                            }
                        }
                    }
                }
            }
        } );
        if ( !getConfiguration().isUseNativeUI() ) {
            _tabbed_pane.setFont( ControlPanel.jcb_font );
        }
        _tabbed_pane.setTabLayoutPolicy( JTabbedPane.SCROLL_TAB_LAYOUT );
        add( _tabbed_pane, BorderLayout.CENTER );
    }

    void setTreeColorSet( final TreeColorSet colorset ) {
        _colorset = colorset;
        for( final TreePanel p : getTreePanels() ) {
            p.setBackground( colorset.getBackgroundColor() );
        }
    }

    void setTreeFontSet( final TreeFontSet fontset ) {
        _fontset = fontset;
    }

    synchronized void setImageMap( final Hashtable<String, BufferedImage> image_map ) {
        _image_map = image_map;
    }

    synchronized Hashtable<String, BufferedImage> getImageMap() {
        return _image_map;
    }
}