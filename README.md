# VMCMC: Visualising MCMC traces #

VMCMC is a statistical graphical software that takes as input Markov Chain Monte
Carlo (MCMC) traces from various softwares, visualize it, and calculates and
displays the statistical properties such as burnin and convergence tests.

### Development ###
VMCMC is written in Java using the Swing library.  The first prototype
developed as Bachelors thesis project by Jorge Miro and Mikael Bark
guided by Lars Arvestad. Further development has been done by Raja
Hashim Ali as part of his PhD project.  Contributors are Raja Manzar
Abbas, Syed Muhammad Zubair, Joel Sj�strand, and Sayyed Auwn Muhammad.

### Contact ###
For suggestions, comments and help regarding VMCMC, please contact Hashim Ali
(rhali at kth dot se) and/or Lars Arvestad (arve at nada dot su dot
se).

### Latest executable jar file ###
The latest executable jar file can be downloaded from [here](https://drive.google.com/file/d/0Bxs0shuzgCXseWduV2hDdVVaT1k/view?usp=sharing).

### Latest tutorial manual ###
The latest tutorial manual can be downloaded from [here](https://drive.google.com/file/d/0Bxs0shuzgCXsTlMwTUQ0ZW8tNlE/view?usp=sharing).