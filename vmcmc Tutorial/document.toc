\contentsline {section}{\numberline {1}INTRODUCTION}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Visual Markov Chain Monte Carlo}{3}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Input}{3}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Output for command line}{3}{subsubsection.1.1.2}
\contentsline {section}{\numberline {2}DOWNLOAD}{4}{section.2}
\contentsline {section}{\numberline {3}REQUIREMENTS}{5}{section.3}
\contentsline {section}{\numberline {4}REFERENCES}{6}{section.4}
\contentsline {section}{\numberline {5}INPUT}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}MCMC File}{7}{subsection.5.1}
\contentsline {section}{\numberline {6}OUTPUT}{8}{section.6}
\contentsline {section}{\numberline {7}RUNNING}{9}{section.7}
\contentsline {subsection}{\numberline {7.1}Starting the application}{9}{subsection.7.1}
\contentsline {section}{\numberline {8}Convergence diagnostics and other analysis tools}{10}{section.8}
\contentsline {subsection}{\numberline {8.1}Alternate to VMCMC!}{10}{subsection.8.1}
\contentsline {section}{\numberline {9}OPTIONS}{11}{section.9}
\contentsline {subsection}{\numberline {9.1}General options}{11}{subsection.9.1}
\contentsline {section}{\numberline {10}GUI}{12}{section.10}
\contentsline {subsection}{\numberline {10.1}First window}{12}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Convergence Diagnostics}{12}{subsection.10.2}
\contentsline {subsection}{\numberline {10.3}Parameter Trace and Statistics}{13}{subsection.10.3}
\contentsline {subsection}{\numberline {10.4}Tabular Representation of Data}{14}{subsection.10.4}
\contentsline {subsection}{\numberline {10.5}Tree Parameter View and Properties}{14}{subsection.10.5}
\contentsline {subsection}{\numberline {10.6}Consensus Tree Properties}{15}{subsection.10.6}
\contentsline {subsection}{\numberline {10.7}Distance between Trees and Multi-Dimensional Scaling}{16}{subsection.10.7}
\contentsline {subsection}{\numberline {10.8}Parallel Chain Analysis for Numeric Parameters}{17}{subsection.10.8}
\contentsline {subsection}{\numberline {10.9}Parallel Chain Analysis for Tree Parameters using Splits}{18}{subsection.10.9}
\contentsline {section}{\numberline {11}TIPS}{20}{section.11}
\contentsline {section}{\numberline {12}EXAMPLES}{22}{section.12}
\contentsline {section}{\numberline {13}FAQ}{23}{section.13}
\contentsline {section}{\numberline {14}Team}{24}{section.14}
